#!/usr/bin/env bash

export USERS_JSON=users.json

cd ./

ruby ./fetchUsers.rb && erb ./sshd_config.erb > sshd_config && cp ./sshd_config /etc/ssh/sshd_config && service ssh restart
