require 'openssl'
require 'redis'
require 'digest'
require 'json'
require 'base64'
require 'net/http'

sshPubKey = File.read(ENV['SSH_KEY_DIR'] + '/' + ENV['SSH_KEY_NAME'] + '.pub')
puts sshPubKey

redis = Redis.new(host: 'redis')

SSH_PUB_KEY_REDIS = 'SSH_PUB_KEY_REDIS_'
alreadyUploadedKey = redis.get SSH_PUB_KEY_REDIS

if alreadyUploadedKey.nil? || alreadyUploadedKey != sshPubKey
  if alreadyUploadedKey.nil?
    puts 'No ssh pub key has been uploaded yet'
  else
    puts 'Already uploaded ssh pub key does not match previously uploaded key'
  end
  STDOUT.flush

  while !File.exists? '/keys/private.pem' do
    puts 'Private key does not exist. Sleeping until it does...'
    STDOUT.flush
    sleep 5
  end

  private_key = OpenSSL::PKey::RSA.new(File.read('/keys/private.pem'))
  
  md5 = Digest::MD5.new
  md5 << sshPubKey
  hash = md5.hexdigest
  puts hash
  
  token = Base64.encode64(private_key.private_encrypt(JSON.generate({createdAt: Time.now.to_i, pubKeyMD5: hash}))).gsub("\n", '')
  puts token
  
  uri = URI('https://proxysockets.api.pennidinh.com/uploadPubKey')
  req = Net::HTTP::Post.new(uri)
  req['x-pennidinh-clientid'] = ENV['PENNIDINH_CLIENT_ID']
  req['x-pennidinh-token'] = token
  req.body = sshPubKey
  http = Net::HTTP.new(uri.hostname, uri.port)
  http.use_ssl = true
  res = http.request(req)
  puts 'upload ssh pub key response: ' + res.body
  STDOUT.flush
  if (res.code != '200')
    raise 'Received ' + res.code + ' for upload ssh pub key request!'
  end

  redis.setex SSH_PUB_KEY_REDIS, 60 * 60, sshPubKey
else
  puts 'Ssh pub key has already been uploaded'
end
