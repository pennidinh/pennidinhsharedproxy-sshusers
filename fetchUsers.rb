# gem install aws-sdk-dynamodb
require 'json'
require 'aws-sdk-lambda'

def getData(service)
  lambdaAws = Aws::Lambda::Client.new(:region => 'us-west-2')
  results = []
  last_eval_key = nil
  loop do
    result = lambdaAws.invoke({:payload => JSON.generate({'service' => service, 'startKey' => last_eval_key, 'sharedProxyDomainName' => ENV['EXTERNAL_DOMAIN_NAME']}), :function_name => ENV['INTERNAL_SHARED_PROXY_ASIGNEES_LAMBDA_NAME']})
    resultJson = JSON.parse(result.payload.string)

    results.concat resultJson['results']

    break if resultJson['nextStartKey'].nil?
    last_eval_key = resultJson['nextStartKey']
  end
  return results
end

sshData = getData('ssh')
httpsData = getData('https')

puts JSON.generate(sshData)
puts JSON.generate(httpsData)

externalHostNameToPorts = {}
for item in sshData do
  if !externalHostNameToPorts.has_key? item['externalSubdomain']
    externalHostNameToPorts[item['externalSubdomain']] = []
  end
  externalHostNameToPorts[item['externalSubdomain']].push(item['port'])
end
for item in httpsData do
  if !externalHostNameToPorts.has_key? item['externalSubdomain']
    externalHostNameToPorts[item['externalSubdomain']] = []
  end
  externalHostNameToPorts[item['externalSubdomain']].push(item['port'])
end
puts JSON.generate(externalHostNameToPorts)

puts externalHostNameToPorts
File.open(ENV['USERS_JSON'], 'w') { |file| file.write(JSON.generate(externalHostNameToPorts)) }


externalHostNameToPubKey = {}
for item in sshData do
  externalHostNameToPubKey[item['externalSubdomain']] = item['sshPubKey']
end
for item in httpsData do
  externalHostNameToPubKey[item['externalSubdomain']] = item['sshPubKey']
end
puts JSON.generate(externalHostNameToPubKey)

for externalHostName in externalHostNameToPubKey.keys do
  authorizedKey = externalHostNameToPubKey[externalHostName]
  puts authorizedKey

  puts `useradd -m #{externalHostName}`
  puts `mkdir -p /home/#{externalHostName}/.ssh`
  File.open("/home/#{externalHostName}/.ssh/authorized_keys", 'w') { |file| file.write(authorizedKey) }
  puts `chmod 600 /home/#{externalHostName}/.ssh/authorized_keys`
  puts `chown #{externalHostName} /home/#{externalHostName}/.ssh/authorized_keys`
end
