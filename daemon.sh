#!/usr/bin/env bash

rm /etc/ssh/ssh_host_*

dpkg-reconfigure openssh-server

ruby ./uploadPubSshKey.rb

/run.sh || exit

sleep 120

while true; do /run.sh; sleep 120; done
