FROM debian:stretch

RUN apt-get update && apt-get install -y ruby openssh-server
RUN gem install aws-sdk-lambda redis
RUN mkdir /var/run/sshd

# SSH login fix. Otherwise user is kicked off after login
#RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

COPY ./daemon.sh /
COPY ./run.sh /
COPY ./sshd_config.erb /
COPY ./fetchUsers.rb /
COPY ./uploadPubSshKey.rb /

CMD /daemon.sh

